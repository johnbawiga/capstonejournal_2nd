import CreateJournalV2 from "./screens/CreateJournalV2";
import DisplayJournal from "./screens/DisplayJournals";
import EditJournalV2 from "./screens/EditJournalV2";
import JournalList from "./screens/JournalList";

import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
     <Stack.Navigator>
        <Stack.Screen
          
          name="JournalList"
          component={JournalList}
          options={{ title: "JOURNALS", headerTitleStyle: {
      color: "#30d5c8" // Set the desired font color here
    } }}
        />
        <Stack.Screen
          name="EditJournalV2"
          component={EditJournalV2}
          options={{ title: "EDIT JOURNAL"  , headerTitleStyle: {
      color: "#30d5c8" // Set the desired font color here
    } }}
        />
        <Stack.Screen
          name="CreateJournalV2"
          component={CreateJournalV2}
          options={{ title: "CREATE JOURNAL", headerTitleStyle: {
      color: "#30d5c8" // Set the desired font color here
    } }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
