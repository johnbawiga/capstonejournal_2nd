import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import * as SQLite from 'expo-sqlite';

import { useNavigation } from '@react-navigation/native';

export default function DisplayJournal() {

    
const [isLoading, setIsLoading] = useState(true);
useEffect(() => {
    // Create the "journal" table if it doesn't exist
    db.transaction((tx) => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS journal (id INTEGER PRIMARY KEY AUTOINCREMENT, Type TEXT, Date TEXT, Mood TEXT, Title TEXT, Message TEXT)',
        [],
        () => {
          console.log('Table created successfully!');
          setIsLoading(false);
        },
        (txObj, error) => {
          console.log('Error creating table:', error);
          setIsLoading(false);
        }
      );
    });
  }, []);
  const db = SQLite.openDatabase('CapstoneJournal.db');
  const [entries, setEntries] = useState([]);
  const navigation = useNavigation();

  useEffect(() => {
    fetchJournalEntries();

    // Create the "journal" table if it doesn't exist
    db.transaction((tx) => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS journal (id INTEGER PRIMARY KEY AUTOINCREMENT, Type TEXT, Date TEXT, Mood TEXT, Title TEXT, Message TEXT)',
        [],
        () => {
          console.log('Table created successfully!');
          setIsLoading(false);
        },
        (txObj, error) => {
          console.log('Error creating table:', error);
          setIsLoading(false);
        }
      );
    });
  }, []);

  const fetchJournalEntries = () => {
    db.transaction((tx) => {
      tx.executeSql(
        'SELECT * FROM journal ORDER BY id DESC',
        [],
        (txObj, resultSet) => {
          const entriesArray = [];
          for (let i = 0; i < resultSet.rows.length; i++) {
            const entry = resultSet.rows.item(i);
            entriesArray.push(entry);
          }
          setEntries(entriesArray);
        },
        (txObj, error) => console.log(error)
      );
    });
  };

  const navigateToCreateJournal = () => {
    navigation.navigate('CreateJournal');
  };

  const navigateToEditJournal = (entry) => {
    navigation.navigate('EditJournal', { entry });
  };

  return (
    <View style={styles.container}>
      {entries.length === 0 ? (
        <Text>No journal entries found.</Text>
      ) : (
        entries.map((entry) => (
          <View key={entry.id} style={styles.entryContainer}>
            <Text style={styles.entryTitle}>{entry.Title}</Text>
            <Text style={styles.entryType}>Type: {entry.Type}</Text>
            <Text style={styles.entryDate}>{entry.Date}</Text>
            <Text style={styles.entryMood}>{entry.Mood}</Text>
            <Text style={styles.entryMessage}>{entry.Message}</Text>
            <TouchableOpacity
              style={styles.editButton}
              onPress={() => navigateToEditJournal(entry)}
            >
              <Text style={styles.editButtonText}>Edit</Text>
            </TouchableOpacity>
          </View>
        ))
      )}
      <TouchableOpacity
        style={styles.addButton}
        onPress={navigateToCreateJournal}
      >
        <Ionicons name="ios-add" size={36} color="white" />
      </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    padding: 20,
  },
  entryContainer: {
    marginBottom: 20,
    borderBottomWidth: 1,
    borderBottomColor: 'gray',
  },
  entryTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  entryType: {
    marginBottom: 5,
  },
  entryDate: {
    color: 'gray',
    marginBottom: 5,
  },
  entryMood: {
    marginBottom: 5,
  },
  entryMessage: {
    marginBottom: 10,
  },
  addButton: {
    position: 'absolute',
    bottom: 20,
    right: 20,
    backgroundColor: 'blue',
    borderRadius: 50,
    width: 60,
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
