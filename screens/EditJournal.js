import React, { useState, useEffect } from 'react';
import { View, TextInput, Button, Switch, StyleSheet, DatePickerIOS, Text } from 'react-native';
import * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase('CapstoneJournal.db');

const EditJournal = ({ route, navigation }) => {
  const { entry } = route.params;
  const [type, setType] = useState('');
  const [date, setDate] = useState(new Date());
  const [mood, setMood] = useState('');
  const [title, setTitle] = useState('');
  const [message, setMessage] = useState('');

  useEffect(() => {
    setType(entry.Type);
    setDate(new Date(entry.Date));
    setMood(entry.Mood);
    setTitle(entry.Title);
    setMessage(entry.Message);
  }, [entry]);

  const updateEntry = () => {
    db.transaction((tx) => {
      tx.executeSql(
        'UPDATE journal SET Type = ?, Date = ?, Mood = ?, Title = ?, Message = ? WHERE id = ?',
        [type, date.toISOString(), mood, title, message, entry.id],
        () => {
          navigation.goBack();
        },
        (txObj, error) => {
          console.log(error);
        }
      );
    });
  };

  return (
    <View style={styles.container}>
      <View style={styles.row}>
        <Switch
          value={type === 'Public'}
          onValueChange={(value) => setType(value ? 'Public' : 'Private')}
        />
        <Text style={styles.label}>{type}</Text>
      </View>
      <DatePickerIOS
        style={styles.datePicker}
        date={date}
        onDateChange={setDate}
        mode="date"
      />
      <TextInput
        style={styles.input}
        value={mood}
        onChangeText={setMood}
        placeholder="Mood"
      />
      <TextInput
        style={styles.input}
        value={title}
        onChangeText={setTitle}
        placeholder="Title"
      />
      <TextInput
        style={styles.input}
        value={message}
        onChangeText={setMessage}
        placeholder="Message"
        multiline
      />
      <Button title="Update" onPress={updateEntry} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  label: {
    marginLeft: 10,
    fontSize: 16,
  },
  datePicker: {
    marginBottom: 20,
  },
  input: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    paddingVertical: 8,
    paddingHorizontal: 10,
    marginBottom: 20,
  },
});

export default EditJournal;
